load "/usr/share/Macaulay2/make-M2-symbols.m2"

symbolsForCodeMirror = template -> (
    output := replace("@M2KEYWORDS@", demark("|", KEYWORDS), template);
    output = replace("@M2DATATYPES@", demark("|", DATATYPES), output);
    output = replace("@M2FUNCTIONS@", demark("|", FUNCTIONS), output);
    output)

-- copied from make-M2-symbols.m2, since it's local to that file
generateGrammar := (grammarFile, grammarFunction) -> (
    template := currentFileDirectory | grammarFile | ".in";
    if fileExists template then (
        stdio << "-- Generating " << grammarFile << endl;
        directory := replace("/[^/].*$", "", grammarFile);
        if not isDirectory directory then makeDirectory directory;
        grammarFile << grammarFunction get(template) << close)
    else stderr << "Skipping generation of " << grammarFile << " as it does not exist." << endl;)

generateGrammar("./macaulay2.js", symbolsForCodeMirror);
